﻿using AutoMapper;
using School.Application.Entities.Requests;
using School.Application.Mapping;
using School.Domain.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace School.Tests.Application.Mapping
{
    public class ApplicationMappingProfileTests
    {
        [Fact]
        public void MapStudentRequestToStudent_GivenInstances_MappedExpecetedly()
        {
            // Arrange
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ApplicationMappingProfile());
            });
            var mapper = config.CreateMapper();

            var expectedName = "Test";
            var expectedLastName = "Surname";

            var sReq = new StudentRequest()
            {
                FirstName = expectedName,
                Surname = expectedLastName
            };
            var expectedStudent = new Student();

            // Act
            var result = mapper.Map<Student>(sReq);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedName, result.FirstName);
            Assert.Equal(expectedLastName, result.Surname);
        }
    }
}
