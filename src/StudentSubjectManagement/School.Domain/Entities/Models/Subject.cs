﻿using School.Domain.Interfaces;
using System.Collections.Generic;

namespace School.Domain.Entities.Models
{
    public class Subject : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CurrentTeacherId { get; set; }

        public Teacher CurrentTeacher { get; set; }
        public IList<Student> Students { get; set; }
        public IList<StudentSubject> StudentsSubjects { get; set; }
    }
}
