﻿using School.Domain.Interfaces;
using System.Collections.Generic;

namespace School.Domain.Entities.Models
{
    public class Teacher : IEntity
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }

        public IList<Subject> Subjects { get; set; }
    }
}
