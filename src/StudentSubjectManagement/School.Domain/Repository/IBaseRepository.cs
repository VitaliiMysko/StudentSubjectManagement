﻿using School.Domain.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace School.Domain.Repository
{
    public interface IBaseRepository<T> where T: class, IEntity
    {
        public Task<List<T>> GetAll();
        public Task<T> Get(int id);
        public Task<T> Add(T entity);
        public Task<T> Update(T entity);
        public Task<T> Delete(int id);
    }
}
