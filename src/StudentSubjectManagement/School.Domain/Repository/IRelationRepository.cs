﻿using System.Threading.Tasks;

namespace School.Domain.Repository
{
    public interface IRelationRepository<T> where T : class
    {
        public Task<T> GetRelation(int firstId, int secondId);
        public Task<T> AddRelation(int firstId, int secondId);
        public Task<T> DeleteRelation(int firstId, int secondId);
    }
}
