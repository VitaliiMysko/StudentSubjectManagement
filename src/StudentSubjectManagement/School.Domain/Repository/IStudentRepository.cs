﻿using School.Domain.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace School.Domain.Repository
{
    public interface IStudentRepository : IBaseRepository<Student>, IRelationRepository<StudentSubject>
    {
        public Task<List<Subject>> GetSubjectsByStudent(int id);
        public Task<List<Teacher>> GetAllTeachersByStudent(int id);
    }
}
