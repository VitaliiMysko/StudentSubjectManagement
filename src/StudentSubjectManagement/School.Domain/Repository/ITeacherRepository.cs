﻿using School.Domain.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace School.Domain.Repository
{
    public interface ITeacherRepository : IBaseRepository<Teacher>
    {
        public Task<List<Subject>> GetAllSubjectsByTeacher(int id);
    }
}
