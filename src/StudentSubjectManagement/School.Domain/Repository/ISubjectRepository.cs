﻿using School.Domain.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace School.Domain.Repository
{
    public interface ISubjectRepository : IBaseRepository<Subject>, IRelationRepository<StudentSubject>
    {
        public Task<List<Student>> GetStudentsBySubject(int id);
        public Task<List<Teacher>> GetAllTeachersBySubject(int id);
    }
}
