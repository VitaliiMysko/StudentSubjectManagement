﻿using Microsoft.EntityFrameworkCore;
using School.Domain.Entities.Models;
using School.Infrastructure.EntityTypeConfiguration;

namespace School.Infrastructure.DataContext
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            new TeacherEntityTypeConfiguration().Configure(modelBuilder.Entity<Teacher>());
            new StudentEntityTypeConfiguration().Configure(modelBuilder.Entity<Student>());
            new SubjectEntityTypeConfiguration().Configure(modelBuilder.Entity<Subject>());
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<StudentSubject> StudentsSubjects { get; set; }
    }
}
