﻿using FluentValidation;
using School.Domain.Entities.Models;
using School.Infrastructure.Validators.RuleSets;

namespace School.Infrastructure.Validators
{
    public class TeacherValidator : AbstractValidator<Teacher>
    {
        public TeacherValidator()
        {
            RuleSet(TeacherRuleSet.Names, () =>
            {
                RuleFor(t => t.FirstName).MinimumLength(2);
                RuleFor(t => t.Surname).MinimumLength(2);
            });
        }
    }
}
