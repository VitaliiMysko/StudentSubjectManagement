﻿using FluentValidation;
using School.Domain.Entities.Models;
using School.Infrastructure.Validators.RuleSets;

namespace School.Infrastructure.Validators
{
    public class StudentValidator : AbstractValidator<Student>
    {
        public StudentValidator()
        {
            RuleSet(StudentRuleSet.Names, () =>
            {
                RuleFor(st => st.FirstName).MinimumLength(2);
                RuleFor(st => st.Surname).MinimumLength(2);
            });
        }
    }
}
