﻿using FluentValidation;
using School.Domain.Entities.Models;
using School.Infrastructure.Validators.RuleSets;

namespace School.Infrastructure.Validators
{
    public class SubjectValidator : AbstractValidator<Subject>
    {
        public SubjectValidator()
        {
            RuleSet(SubjectRuleSet.Names, () =>
            {
                RuleFor(sb => sb.Name).MinimumLength(2);
            });

            RuleSet(SubjectRuleSet.Relationship, () =>
            {
                RuleFor(sb => sb.CurrentTeacherId).GreaterThan(0);
            });
        }
    }
}
