﻿
namespace School.Infrastructure.Validators.RuleSets
{
    public static class SubjectRuleSet
    {
        public static readonly string Names = "Names";
        public static readonly string Relationship = "Relationship";
    }
}
