﻿using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.EntityFrameworkCore;
using School.Domain.Entities.Models;
using School.Domain.Repository;
using School.Infrastructure.Validators.RuleSets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Infrastructure.Repository
{
    public class SubjectsRepository<TContext> : ISubjectRepository
        where TContext : DbContext
    {
        private readonly TContext _context;
        private readonly IValidator<Subject> _validator;
        private readonly IMapper _mapper;

        public SubjectsRepository(TContext context,
            IValidator<Subject> validator,
            IMapper mapper)
        {
            _context = context;
            _validator = validator;
            _mapper = mapper;
        }
        public async Task<List<Subject>> GetAll()
        {
            var subjects = await _context.Set<Subject>()
                .Include(sb => sb.CurrentTeacher)
                .Include(sb => sb.Students)
                .ToListAsync();

            return subjects;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Subject> Get(int id)
        {
            var subject = await _context.Set<Subject>()
                .Where(sb => sb.Id == id)
                .Include(sb => sb.CurrentTeacher)
                .Include(sb => sb.Students)
                .FirstOrDefaultAsync();

            return subject;
        }

        public async Task<Subject> Update(Subject subject)
        {
            ValidationResult results = DoValidation(subject);

            if (!results.IsValid)
            {
                throw new Exception(results.ToString());
            }

            _context.Entry(subject).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return subject;
        }

        public async Task<Subject> Add(Subject subject)
        {
            ValidationResult results = DoValidation(subject);

            if (!results.IsValid)
            {
                throw new Exception(results.ToString());
            }

            _context.Set<Subject>()
                .Add(subject);

            await _context.SaveChangesAsync();

            return subject;
        }

        public async Task<Subject> Delete(int id)
        {
            var subject = await _context.Set<Subject>()
                .FindAsync(id);

            if (subject == null)
            {
                return subject;
            }

            _context.Set<Subject>()
                .Remove(subject);

            await _context.SaveChangesAsync();

            return subject;
        }

        private ValidationResult DoValidation(Subject subject)
        {
            return _validator.Validate(subject, options =>
            {
                options.IncludeRuleSets(SubjectRuleSet.Names, SubjectRuleSet.Relationship);
            });
        }


        public async Task<List<Student>> GetStudentsBySubject(int id)
        {
            var students = await _context.Set<Subject>()
                .Where(sb => sb.Id == id)
                .SelectMany(sb => sb.Students)
                .Include(st => st.Subjects)
                .ThenInclude(sb => sb.CurrentTeacher)
                .Include(st => st.Subjects)
                .ThenInclude(sb => sb.Students)
                .ToListAsync();

            return students;
        }

        public async Task<List<Teacher>> GetAllTeachersBySubject(int id)
        {
            var teachers = await _context.Set<Subject>()
                .Where(sb => sb.Id == id)
                .Select(sb => new Teacher
                {
                    Id = sb.CurrentTeacherId,
                    FirstName = sb.CurrentTeacher.FirstName,
                    Surname = sb.CurrentTeacher.Surname,
                    Subjects = sb.CurrentTeacher.Subjects
                })
                .ToListAsync();

            return teachers;
        }


        public async Task<StudentSubject> GetRelation(int studentId, int subjectId)
        {
            var sS = await _context.Set<StudentSubject>()
                .Where(ss => ss.StudentId == studentId)
                .Where(ss => ss.SubjectId == subjectId)
                .Include(ss => ss.Student)
                .Include(ss => ss.Subject)
                .ThenInclude(sb => sb.CurrentTeacher)
                .FirstOrDefaultAsync();

            return sS;
        }

        public async Task<StudentSubject> AddRelation(int studentId, int subjectId)
        {
            var student = await _context.Set<Student>()
                .FirstOrDefaultAsync(st => st.Id == studentId);
            var subject = await _context.Set<Subject>()
                .Where(sb => sb.Id == subjectId)
                .Include(sb => sb.Students)
                .FirstOrDefaultAsync();

            //string error = "";

            //if (student == null)
            //{
            //    error = $"Student not found with Id {studentId}";
            //}
            //if (subject == null)
            //{
            //    string insideError = $"Subject not found with Id {subjectId}";
            //    error += error == String.Empty ? insideError : $"\n{insideError}";
            //}

            //if (error != String.Empty)
            //{
            //    throw new Exception(error);
            //}

            var sS = await _context.Set<StudentSubject>()
                .FindAsync(studentId, subjectId);

            if (sS == null)
            {
                subject.Students.Add(student);

                await _context.SaveChangesAsync();

                sS = await _context.Set<StudentSubject>()
                    .FindAsync(studentId, subjectId);
            }

            return sS;
        }

        public async Task<StudentSubject> DeleteRelation(int studentId, int subjectId)
        {
            var sS = await _context.Set<StudentSubject>()
                .FindAsync(studentId, subjectId);

            if (sS == null)
            {
                return sS;
            }

            _context.Set<StudentSubject>()
                .Remove(sS);

            await _context.SaveChangesAsync();

            return sS;
        }

    }
}
