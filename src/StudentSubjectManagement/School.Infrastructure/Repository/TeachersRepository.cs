﻿using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.EntityFrameworkCore;
using School.Domain.Entities.Models;
using School.Domain.Repository;
using School.Infrastructure.Validators.RuleSets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Infrastructure.Repository
{
    public class TeachersRepository<TContext> : ITeacherRepository
        where TContext : DbContext
    {
        private readonly TContext _context;
        private readonly IValidator<Teacher> _validator;
        private readonly IMapper _mapper;

        public TeachersRepository(TContext context,
            IValidator<Teacher> validator,
            IMapper mapper)
        {
            _context = context;
            _validator = validator;
            _mapper = mapper;
        }

        public async Task<List<Teacher>> GetAll()
        {
            var teachers = await _context.Set<Teacher>()
                .Include(sb => sb.Subjects)
                .ThenInclude(sb => sb.Students)
                .ToListAsync();

            return teachers;
        }

        public async Task<Teacher> Get(int id)
        {
            var teacher = await _context.Set<Teacher>()
                .Where(t => t.Id == id)
                .Include(t => t.Subjects)
                .ThenInclude(sb => sb.Students)
                .FirstOrDefaultAsync();

            return teacher;
        }

        public async Task<Teacher> Update(Teacher teacher)
        {
            ValidationResult results = DoValidation(teacher);

            if (!results.IsValid)
            {
                throw new Exception(results.ToString());
            }

            _context.Entry(teacher).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return teacher;
        }

        public async Task<Teacher> Add(Teacher teacher)
        {
            ValidationResult results = DoValidation(teacher);

            if (!results.IsValid)
            {
                throw new Exception(results.ToString());
            }

            _context.Set<Teacher>()
                .Add(teacher);

            await _context.SaveChangesAsync();

            return teacher;
        }

        public async Task<Teacher> Delete(int id)
        {
            var teacher = await _context.Set<Teacher>()
                .FindAsync(id);

            if (teacher == null)
            {
                return teacher;
            }

            _context.Set<Teacher>()
                .Remove(teacher);

            await _context.SaveChangesAsync();

            return teacher;
        }

        private ValidationResult DoValidation(Teacher teacher)
        {
            return _validator.Validate(teacher, options =>
            {
                options.IncludeRuleSets(TeacherRuleSet.Names);
            });
        }


        public async Task<List<Subject>> GetAllSubjectsByTeacher(int id)
        {
            var subjects = await _context.Set<Subject>()
                .Where(sb => sb.CurrentTeacherId == id)
                .Include(sb => sb.CurrentTeacher)
                .Include(sb => sb.Students)
                .ToListAsync();

            return subjects;
        }
    }
}
