﻿using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.EntityFrameworkCore;
using School.Domain.Entities.Models;
using School.Domain.Repository;
using School.Infrastructure.Validators.RuleSets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Infrastructure.Repository
{
    public class StudentsRepository<TContext> : IStudentRepository
        where TContext : DbContext
    {
        private readonly TContext _context;
        private readonly IValidator<Student> _validator;
        private readonly IMapper _mapper;

        public StudentsRepository(TContext context,
            IValidator<Student> validator,
            IMapper mapper)
        {
            _context = context;
            _validator = validator;
            _mapper = mapper;
        }

        public async Task<List<Student>> GetAll()
        {
            var students = await _context.Set<Student>()
                .Include(st => st.Subjects)
                .ThenInclude(sb => sb.CurrentTeacher)
                .ToListAsync();

            return students;
        }

        public async Task<Student> Get(int id)
        {
            var student = await _context.Set<Student>()
                .Where(st => st.Id == id)
                .Include(st => st.Subjects)
                .ThenInclude(sb => sb.CurrentTeacher)
                .FirstOrDefaultAsync();

            return student;
        }

        public async Task<Student> Update(Student student)
        {
            ValidationResult results = DoValidation(student);

            if (!results.IsValid)
            {
                throw new Exception(results.ToString());
            }

            _context.Entry(student).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return student;
        }

        public async Task<Student> Add(Student student)
        {
            ValidationResult results = DoValidation(student);

            if (!results.IsValid)
            {
                throw new Exception(results.ToString());
            }

            _context.Set<Student>()
                .Add(student);

            await _context.SaveChangesAsync();

            return student;
        }

        public async Task<Student> Delete(int id)
        {
            var student = await _context.Set<Student>()
                .FindAsync(id);

            if (student == null)
            {
                return student;
            }

            _context.Set<Student>()
                .Remove(student);

            await _context.SaveChangesAsync();

            return student;
        }

        private ValidationResult DoValidation(Student student)
        {
            return _validator.Validate(student, options =>
            {
                options.IncludeRuleSets(StudentRuleSet.Names);
            });
        }


        public async Task<List<Subject>> GetSubjectsByStudent(int id)
        {
            var subjects = await _context.Set<Student>()
                .Where(st => st.Id == id)
                .SelectMany(st => st.Subjects)
                .Include(sb => sb.CurrentTeacher)
                .Include(sb => sb.Students)
                .ToListAsync();

            return subjects;
        }

        public async Task<List<Teacher>> GetAllTeachersByStudent(int id)
        {
            var teachers = await _context.Set<Student>()
                .Where(st => st.Id == id)
                .SelectMany(st => st.Subjects)
                .Select(sb => new Teacher
                {
                    Id = sb.CurrentTeacherId,
                    FirstName = sb.CurrentTeacher.FirstName,
                    Surname = sb.CurrentTeacher.Surname,
                    Subjects = sb.CurrentTeacher.Subjects
                })
                .ToListAsync();

            return teachers;
        }


        public async Task<StudentSubject> GetRelation(int studentId, int subjectId)
        {
            var sS = await _context.Set<StudentSubject>()
                .Where(ss => ss.StudentId == studentId)
                .Where(ss => ss.SubjectId == subjectId)
                .Include(ss => ss.Student)
                .Include(ss => ss.Subject)
                .ThenInclude(sb => sb.CurrentTeacher)
                .FirstOrDefaultAsync();

            return sS;
        }

        public async Task<StudentSubject> AddRelation(int studentId, int subjectId)
        {
            var subject = await _context.Set<Subject>()
                .FirstOrDefaultAsync(st => st.Id == subjectId);

            var student = await _context.Set<Student>()
                .Where(sb => sb.Id == studentId)
                .Include(sb => sb.Subjects)
                .FirstOrDefaultAsync();

            var sS = await _context.Set<StudentSubject>()
                .FindAsync(studentId, subjectId);

            if (sS == null)
            {
                student.Subjects.Add(subject);

                await _context.SaveChangesAsync();

                sS = await _context.Set<StudentSubject>()
                    .FindAsync(studentId, subjectId);
            }

            return sS;
        }

        public async Task<StudentSubject> DeleteRelation(int id, int subjectId)
        {
            var sS = await _context.Set<StudentSubject>()
                .FindAsync(id, subjectId);

            if (sS == null)
            {
                return sS;
            }

            _context.Set<StudentSubject>()
                .Remove(sS);

            await _context.SaveChangesAsync();

            return sS;
        }
    }

}
