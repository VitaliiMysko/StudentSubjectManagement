﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using School.Domain.Entities.Models;

namespace School.Infrastructure.EntityTypeConfiguration
{
    public class StudentEntityTypeConfiguration : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder
                .Property(t => t.FirstName)
                .IsRequired();

            builder
                .Property(t => t.Surname)
                .IsRequired();

            builder
                .HasData(
                    new Student { Id = 1, FirstName = "Student 1", Surname = "sur Student 1" },
                    new Student { Id = 2, FirstName = "Student 2", Surname = "sur Student 2" },
                    new Student { Id = 3, FirstName = "Student 3", Surname = "sur Student 3" }
                );
        }
    }
}
