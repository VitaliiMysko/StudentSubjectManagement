﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using School.Domain.Entities.Models;

namespace School.Infrastructure.EntityTypeConfiguration
{
    public class SubjectEntityTypeConfiguration : IEntityTypeConfiguration<Subject>
    {
        public void Configure(EntityTypeBuilder<Subject> builder)
        {
            builder
                .Property(sb => sb.Name)
                .IsRequired();

            builder
                .HasData(
                    new Subject { Id = 1, Name = "Subject 1", CurrentTeacherId = 1 },
                    new Subject { Id = 2, Name = "Subject 2", CurrentTeacherId = 2 },
                    new Subject { Id = 3, Name = "Subject 3", CurrentTeacherId = 2 }
                );

            builder
                .HasOne(sb => sb.CurrentTeacher)
                .WithMany(t => t.Subjects)
                .HasForeignKey(sb => sb.CurrentTeacherId)
                .IsRequired();

            builder
                .HasMany(sb => sb.Students)
                .WithMany(st => st.Subjects)
                .UsingEntity<StudentSubject>(
                    y => y
                        .HasOne(ss => ss.Student)
                        .WithMany(st => st.StudentsSubjects)
                        .HasForeignKey(st => st.StudentId),
                    y => y
                        .HasOne(ss => ss.Subject)
                        .WithMany(sb => sb.StudentsSubjects)
                        .HasForeignKey(sb => sb.SubjectId)
                  , y => y
                        .HasData(
                             new StudentSubject { StudentId = 1, SubjectId = 1 },
                             new StudentSubject { StudentId = 1, SubjectId = 2 },
                             new StudentSubject { StudentId = 2, SubjectId = 2 }
                         )
                );
        }
    }
}
