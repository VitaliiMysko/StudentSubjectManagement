﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using School.Domain.Entities.Models;

namespace School.Infrastructure.EntityTypeConfiguration
{
    public class TeacherEntityTypeConfiguration : IEntityTypeConfiguration<Teacher>
    {
        public void Configure(EntityTypeBuilder<Teacher> builder)
        {
            builder
                .Property(t => t.FirstName)
                .IsRequired();

            builder
                .Property(t => t.Surname)
                .IsRequired();

            builder
                .HasData(
                    new Teacher { Id = 1, FirstName = "Teacher 1", Surname = "sur Teacher 1" },
                    new Teacher { Id = 2, FirstName = "Teacher 2", Surname = "sur Teacher 2" },
                    new Teacher { Id = 3, FirstName = "Teacher 3", Surname = "sur Teacher 3" }
                );
        }
    }
}
