﻿
namespace School.Application.Entities.Responses
{
    public class TeacherResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
    }
}
