﻿
namespace School.Application.Entities.Responses
{
    public class StudentSubjectResponse
    {
        public int StudentId { get; set; }
        public int SubjectId { get; set; }
    }
}
