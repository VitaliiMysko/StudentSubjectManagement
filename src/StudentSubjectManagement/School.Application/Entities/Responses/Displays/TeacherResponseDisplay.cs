﻿using System.Collections.Generic;

namespace School.Application.Entities.Responses.Displays
{
    public class TeacherResponseDisplay : TeacherResponse
    {
        public IList<SubjectResponse> Subjects { get; set; }
    }
}
