﻿
namespace School.Application.Entities.Responses.Displays
{
    public class StudentSubjectResponseDisplay : StudentSubjectResponse
    {
        public StudentResponse Student { get; set; }
        public SubjectResponse Subject { get; set; }
    }
}
