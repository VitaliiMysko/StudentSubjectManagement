﻿using System.Collections.Generic;

namespace School.Application.Entities.Responses.Displays
{
    public class SubjectResponseDisplay : SubjectResponse
    {
        public TeacherResponse CurrentTeacher { get; set; }
        public IList<StudentResponse> Students { get; set; }
    }
}
