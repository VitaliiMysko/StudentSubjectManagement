﻿using System.Collections.Generic;

namespace School.Application.Entities.Responses.Displays
{
    public class StudentResponseDisplay : StudentResponse
    {
        public IList<SubjectResponse> Subjects { get; set; }
    }
}
