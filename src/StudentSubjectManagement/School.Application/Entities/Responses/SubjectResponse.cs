﻿
namespace School.Application.Entities.Responses
{
    public class SubjectResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CurrentTeacherId { get; set; }
    }
}
