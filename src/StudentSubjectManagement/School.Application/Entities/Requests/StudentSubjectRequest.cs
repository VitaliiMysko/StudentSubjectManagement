﻿
namespace School.Application.Entities.Requests
{
    public class StudentSubjectRequest
    {
        public int StudentId { get; set; }
        public int SubjectId { get; set; }
    }
}
