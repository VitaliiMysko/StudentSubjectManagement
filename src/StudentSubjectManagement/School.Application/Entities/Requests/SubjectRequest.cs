﻿
namespace School.Application.Entities.Requests
{
    public class SubjectRequest
    {
        public string Name { get; set; }
        public int CurrentTeacherId { get; set; }
    }
}
