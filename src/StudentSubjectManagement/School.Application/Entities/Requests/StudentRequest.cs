﻿
namespace School.Application.Entities.Requests
{
    public class StudentRequest
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
    }
}
