﻿
namespace School.Application.Entities.Requests
{
    public class TeacherRequest
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
    }
}
