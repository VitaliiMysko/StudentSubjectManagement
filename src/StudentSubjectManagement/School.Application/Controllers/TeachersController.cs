﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using School.Application.Entities.Requests;
using School.Application.Entities.Responses;
using School.Application.Entities.Responses.Displays;
using School.Domain.Entities.Models;
using School.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace School.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeachersController : ControllerBase
    {
        private readonly ITeacherRepository _teacherRepository;
        private readonly IMapper _mapper;

        public TeachersController(ITeacherRepository teacherRepository,
            IMapper mapper)
        {
            _teacherRepository = teacherRepository;
            _mapper = mapper;
        }

        // GET: api/teachers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeacherResponseDisplay>>> GetTeachers()
        {
            var teachers = await _teacherRepository.GetAll();
            var teachersDisplay = _mapper.Map<List<TeacherResponseDisplay>>(teachers);
            return teachersDisplay;
        }

        // GET: api/teachers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TeacherResponseDisplay>> GetTeacher(int id)
        {
            var teacher = await _teacherRepository.Get(id);

            if (teacher == null)
            {
                return NotFound();
            }

            var teacherDisplay = _mapper.Map<TeacherResponseDisplay>(teacher);

            return teacherDisplay;
        }

        // PUT: api/teachers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTeacher(int id, TeacherResponse teacherResponse)
        {
            if (id != teacherResponse.Id)
            {
                return BadRequest("Id must be the same; that is, object's Id cannot be change");
            }

            var teacher = _mapper.Map<Teacher>(teacherResponse);

            try
            {
                await _teacherRepository.Update(teacher);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return NoContent();
        }

        // POST: api/teachers
        [HttpPost]
        public async Task<ActionResult<Teacher>> PostTeacher(TeacherRequest teacherRequest)
        {
            var teacherModificate = _mapper.Map<Teacher>(teacherRequest);

            try
            {
                var teacher = await _teacherRepository.Add(teacherModificate);

                return CreatedAtAction("GetTeacher", new { id = teacher.Id }, teacher);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/teachers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTeacher(int id)
        {
            var teacher = await _teacherRepository.Delete(id);

            if (teacher == null)
            {
                return NotFound();
            }

            return NoContent();
        }


        // GET: api/teachers/2/subjects
        [HttpGet("{id}/subjects")]
        public async Task<ActionResult<IEnumerable<SubjectResponseDisplay>>> GetAllSubjectsByTeacher(int id)
        {
            var subjects = await _teacherRepository.GetAllSubjectsByTeacher(id);

            if (subjects.Count == 0)
            {
                return NotFound();
            }

            var subjectsDisplay = _mapper.Map<List<SubjectResponseDisplay>>(subjects);

            return subjectsDisplay;
        }

    }

}
