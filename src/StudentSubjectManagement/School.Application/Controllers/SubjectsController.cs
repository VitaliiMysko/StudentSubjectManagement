﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using School.Application.Entities.Requests;
using School.Application.Entities.Responses;
using School.Application.Entities.Responses.Displays;
using School.Domain.Entities.Models;
using School.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace School.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectsController : ControllerBase
    {
        private readonly ISubjectRepository _subjectRepository;
        private readonly IMapper _mapper;

        public SubjectsController(ISubjectRepository subjectRepository,
            IMapper mapper)
        {
            _subjectRepository = subjectRepository;
            _mapper = mapper;
        }

        // GET: api/subjects
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SubjectResponseDisplay>>> GetSubjects()
        {
            var subjects = await _subjectRepository.GetAll();
            var subjectsDisplay = _mapper.Map<List<SubjectResponseDisplay>>(subjects);
            return subjectsDisplay;
        }

        // GET: api/subjects/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SubjectResponseDisplay>> GetSubject(int id)
        {
            var subject = await _subjectRepository.Get(id);

            if (subject == null)
            {
                return NotFound();
            }

            var subjectDisplay = _mapper.Map<SubjectResponseDisplay>(subject);

            return subjectDisplay;
        }

        // PUT: api/subjects/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubject(int id, SubjectResponse subjectResponse)
        {
            if (id != subjectResponse.Id)
            {
                return BadRequest("Id must be the same; that is, object's Id cannot be change");
            }

            var subject = _mapper.Map<Subject>(subjectResponse);

            try
            {
                await _subjectRepository.Update(subject);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return NoContent();
        }

        // POST: api/subjects
        [HttpPost]
        public async Task<ActionResult<Subject>> PostSubject(SubjectRequest subjectRequest)
        {
            var subjectModificate = _mapper.Map<Subject>(subjectRequest);

            try
            {
                var subject = await _subjectRepository.Add(subjectModificate);

                return CreatedAtAction("GetSubject", new { id = subject.Id }, subject);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/subjects/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSubject(int id)
        {
            var subject = await _subjectRepository.Delete(id);

            if (subject == null)
            {
                return NotFound();
            }

            return NoContent();
        }


        //GET: api/subjects/{id}/students
        [HttpGet("{id}/students")]
        public async Task<ActionResult<IEnumerable<StudentResponseDisplay>>> GetStudentsBySubject(int id)
        {
            var students = await _subjectRepository.GetStudentsBySubject(id);

            if (students.Count == 0)
            {
                return NotFound();
            }

            var studentsDisplay = _mapper.Map<List<StudentResponseDisplay>>(students);

            return studentsDisplay;
        }

        // GET: api/subjects/3/teachers
        [HttpGet("{id}/teachers")]
        public async Task<ActionResult<IEnumerable<TeacherResponseDisplay>>> GetAllTeachersBySubject(int id)
        {
            var teachers = await _subjectRepository.GetAllTeachersBySubject(id);

            if (teachers.Count == 0)
            {
                return NotFound();
            }

            var teachersDisplay = _mapper.Map<List<TeacherResponseDisplay>>(teachers);

            return teachersDisplay;
        }


        // GET: api/subjects/3/students/4
        [HttpGet("{subjectId}/students/{studentId}")]
        public async Task<ActionResult<StudentSubjectResponseDisplay>> GetStudentSubject(int subjectId, int studentId)
        {
            var sS = await _subjectRepository.GetRelation(studentId, subjectId);

            if (sS == null)
            {
                return NotFound();
            }

            var sSDisplay = _mapper.Map<StudentSubjectResponseDisplay>(sS);

            return sSDisplay;
        }

        // POST: api/subjects/3/students/4
        [HttpPost("{subjectId}/students/{studentId}")]
        public async Task<ActionResult<StudentSubjectResponseDisplay>> PostStudentSubject(int subjectId, int studentId)
        {
            try
            {
                var sS = await _subjectRepository.AddRelation(studentId, subjectId);

                var ssDisplay = _mapper.Map<StudentSubjectResponseDisplay>(sS);

                return CreatedAtAction("GetStudentSubject", new { studentId = sS.StudentId, subjectId = sS.SubjectId }, ssDisplay);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/subjects/3/students/5
        [HttpDelete("{subjectId}/students/{studentId}")]
        public async Task<IActionResult> DeleteStudentToSubjectRelation(int subjectId, int studentId)
        {
            var sS = await _subjectRepository.DeleteRelation(studentId, subjectId);

            if (sS == null)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
