﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using School.Application.Entities.Requests;
using School.Application.Entities.Responses;
using School.Application.Entities.Responses.Displays;
using School.Domain.Entities.Models;
using School.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace School.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly IStudentRepository _studentRepository;
        private readonly IMapper _mapper;

        public StudentsController(IStudentRepository studentRepository,
            IMapper mapper)
        {
            _studentRepository = studentRepository;
            _mapper = mapper;
        }

        // GET: api/students
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StudentResponseDisplay>>> GetStudents()
        {
            var students = await _studentRepository.GetAll();
            var studentsDisplay = _mapper.Map<List<StudentResponseDisplay>>(students);
            return studentsDisplay;
        }

        // GET: api/students/5
        [HttpGet("{id}")]
        public async Task<ActionResult<StudentResponseDisplay>> GetStudent(int id)
        {
            var student = await _studentRepository.Get(id);

            if (student == null)
            {
                return NotFound();
            }

            var studentDisplay = _mapper.Map<StudentResponseDisplay>(student);

            return studentDisplay;
        }

        // PUT: api/student/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubject(int id, StudentResponse studentResponse)
        {
            if (id != studentResponse.Id)
            {
                return BadRequest("Id must be the same; that is, object's Id cannot be change");
            }

            var student = _mapper.Map<Student>(studentResponse);

            try
            {
                await _studentRepository.Update(student);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return NoContent();
        }

        // POST: api/students
        [HttpPost]
        public async Task<ActionResult<Student>> PostStudent(StudentRequest studentRequest)
        {
            var studentModificate = _mapper.Map<Student>(studentRequest);

            try
            {
                var student = await _studentRepository.Add(studentModificate);

                return CreatedAtAction("GetStudent", new { id = student.Id }, student);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/students/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStudent(int id)
        {
            var student = await _studentRepository.Delete(id);

            if (student == null)
            {
                return NotFound();
            }

            return NoContent();
        }


        //GET: api/students/2/subjects
        [HttpGet("{id}/subjects")]
        public async Task<ActionResult<IEnumerable<SubjectResponseDisplay>>> GetSubjectsByStudent(int id)
        {
            var subjects = await _studentRepository.GetSubjectsByStudent(id);

            if (subjects.Count == 0)
            {
                return NotFound();
            }

            var subjectsDisplay = _mapper.Map<List<SubjectResponseDisplay>>(subjects);

            return subjectsDisplay;
        }

        // GET: api/students/3/teachers
        [HttpGet("{id}/teachers")]
        public async Task<ActionResult<IEnumerable<TeacherResponseDisplay>>> GetAllTeachersByStudent(int id)
        {
            var teachers = await _studentRepository.GetAllTeachersByStudent(id);

            if (teachers.Count == 0)
            {
                return NotFound();
            }

            var teachersDisplay = _mapper.Map<List<TeacherResponseDisplay>>(teachers);

            return teachersDisplay;
        }


        // GET: api/students/3/subjects/1
        [HttpGet("{studentId}/subjects/{subjectId}")]
        public async Task<ActionResult<StudentSubjectResponseDisplay>> GetStudentSubject(int studentId, int subjectId)
        {
            var sS = await _studentRepository.GetRelation(studentId, subjectId);

            if (sS == null)
            {
                return NotFound();
            }

            var sSDisplay = _mapper.Map<StudentSubjectResponseDisplay>(sS);

            return sSDisplay;
        }

        // POST: api/students/3/subjects/1
        [HttpPost("{studentId}/subjects/{subjectId}")]
        public async Task<ActionResult<StudentSubjectRequest>> PostStudentSubject(int studentId, int subjectId)
        {
            try
            {
                var sS = await _studentRepository.AddRelation(studentId, subjectId);

                var sSDisplay = _mapper.Map<StudentSubjectResponseDisplay>(sS);

                return CreatedAtAction("GetStudentSubject", new { studentId = sS.StudentId, subjectId = sS.SubjectId }, sSDisplay);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/students/2/subject/5
        [HttpDelete("{studentId}/subjects/{subjectId}")]
        public async Task<IActionResult> DeleteStudentToSubjectRelation(int studentId, int subjectId)
        {
            var sS = await _studentRepository.DeleteRelation(studentId, subjectId);

            if (sS == null)
            {
                return NotFound();
            }

            return NoContent();
        }
    }

}
