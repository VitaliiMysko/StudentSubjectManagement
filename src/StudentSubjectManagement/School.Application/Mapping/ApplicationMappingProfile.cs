﻿using AutoMapper;
using School.Application.Entities.Requests;
using School.Application.Entities.Responses;
using School.Application.Entities.Responses.Displays;
using School.Domain.Entities.Models;

namespace School.Application.Mapping
{
    public class ApplicationMappingProfile : Profile
    {
        public ApplicationMappingProfile()
        {
            CreateMap<StudentRequest, Student>()
                .ForMember(x => x.FirstName, x => x.MapFrom(m => m.FirstName))
                .ReverseMap();
            CreateMap<SubjectRequest, Subject>().ReverseMap();
            CreateMap<TeacherRequest, Teacher>().ReverseMap();
            CreateMap<StudentSubjectRequest, StudentSubject>().ReverseMap();

            CreateMap<StudentResponse, Student>().ReverseMap();
            CreateMap<SubjectResponse, Subject>().ReverseMap();
            CreateMap<TeacherResponse, Teacher>().ReverseMap();
            CreateMap<StudentSubjectResponse, StudentSubject>().ReverseMap();

            CreateMap<StudentResponseDisplay, Student>().ReverseMap();
            CreateMap<SubjectResponseDisplay, Subject>().ReverseMap();
            CreateMap<TeacherResponseDisplay, Teacher>().ReverseMap();
            CreateMap<StudentSubjectResponseDisplay, StudentSubject>().ReverseMap();
        }
    }
}
