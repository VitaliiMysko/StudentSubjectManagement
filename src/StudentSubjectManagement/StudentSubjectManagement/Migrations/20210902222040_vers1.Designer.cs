﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using School.Infrastructure.DataContext;

namespace School.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20210902222040_vers1")]
    partial class vers1
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.9")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("School.Domain.Entities.Student", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Surname")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Students");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            FirstName = "Student 1",
                            Surname = "sur Student 1"
                        },
                        new
                        {
                            Id = 2,
                            FirstName = "Student 2",
                            Surname = "sur Student 2"
                        },
                        new
                        {
                            Id = 3,
                            FirstName = "Student 3",
                            Surname = "sur Student 3"
                        });
                });

            modelBuilder.Entity("School.Domain.Entities.StudentSubject", b =>
                {
                    b.Property<int>("StudentId")
                        .HasColumnType("int");

                    b.Property<int>("SubjectId")
                        .HasColumnType("int");

                    b.HasKey("StudentId", "SubjectId");

                    b.HasIndex("SubjectId");

                    b.ToTable("StudentsSubjects");

                    b.HasData(
                        new
                        {
                            StudentId = 1,
                            SubjectId = 1
                        },
                        new
                        {
                            StudentId = 1,
                            SubjectId = 2
                        },
                        new
                        {
                            StudentId = 2,
                            SubjectId = 2
                        });
                });

            modelBuilder.Entity("School.Domain.Entities.Subject", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CurrentTeacherId")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("CurrentTeacherId");

                    b.ToTable("Subjects");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CurrentTeacherId = 1,
                            Name = "Subject 1"
                        },
                        new
                        {
                            Id = 2,
                            CurrentTeacherId = 2,
                            Name = "Subject 2"
                        },
                        new
                        {
                            Id = 3,
                            CurrentTeacherId = 2,
                            Name = "Subject 3"
                        });
                });

            modelBuilder.Entity("School.Domain.Entities.Teacher", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Surname")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Teachers");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            FirstName = "Teacher 1",
                            Surname = "sur Teacher 1"
                        },
                        new
                        {
                            Id = 2,
                            FirstName = "Teacher 2",
                            Surname = "sur Teacher 2"
                        },
                        new
                        {
                            Id = 3,
                            FirstName = "Teacher 3",
                            Surname = "sur Teacher 3"
                        });
                });

            modelBuilder.Entity("School.Domain.Entities.StudentSubject", b =>
                {
                    b.HasOne("School.Domain.Entities.Student", "Student")
                        .WithMany("StudentsSubjects")
                        .HasForeignKey("StudentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("School.Domain.Entities.Subject", "Subject")
                        .WithMany("StudentsSubjects")
                        .HasForeignKey("SubjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Student");

                    b.Navigation("Subject");
                });

            modelBuilder.Entity("School.Domain.Entities.Subject", b =>
                {
                    b.HasOne("School.Domain.Entities.Teacher", "CurrentTeacher")
                        .WithMany("Subjects")
                        .HasForeignKey("CurrentTeacherId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("CurrentTeacher");
                });

            modelBuilder.Entity("School.Domain.Entities.Student", b =>
                {
                    b.Navigation("StudentsSubjects");
                });

            modelBuilder.Entity("School.Domain.Entities.Subject", b =>
                {
                    b.Navigation("StudentsSubjects");
                });

            modelBuilder.Entity("School.Domain.Entities.Teacher", b =>
                {
                    b.Navigation("Subjects");
                });
#pragma warning restore 612, 618
        }
    }
}
